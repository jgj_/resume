(function() {

    var btnPrint = document.querySelector(".btn-print"),
        btnPDF = document.querySelector(".btn-pdf"),
        btnSend = document.querySelector(".btn-send"),
        btnContact = document.querySelector(".btn-contact"),
        sendDialog = document.querySelector(".send-dialog"),
        contactDialog = document.querySelector(".contact-dialog"),
        sendForm = document.querySelector(".send-form"),
        contactForm = document.querySelector(".contact-form"),
        now, loader;


    document.addEventListener("DOMContentLoaded", function(event) { 
        setDateString();
        initEvents();
        initNotifications();
    });


    function initNotifications() {
        humane.loading = humane.spawn({addnClas: 'loading', timeout: 0});
        humane.info = humane.spawn({addnCls: 'info', clickToClose: true, timeout: 3000});
        humane.success = humane.spawn({addnCls: 'success', clickToClose: true, timeout: 3000});
        humane.error = humane.spawn({addnCls: 'error', clickToClose: true, timeout: 3000});
    }


    function setDateString() {
        now = moment();
        document.querySelector(".date").innerHTML = now.format("dddd [the] Do [of] MMMM, YYYY");
    }


    function initEvents() {
        btnPrint.addEventListener("click", function(event) {
            //window.print();

            //humane.loading('<i class="fa fa-spinner fa-spin"></i> Loading');
            showLoading();

            //humane.info("This is a info notification");
            //humane.success("This is a success notification");
            //humane.error("This is a error notification");

        });

        btnPDF.addEventListener("click", function(event) {
            window.location.assign("/resume/pdf/");
        });

        btnSend.addEventListener("click", function(event) { openDialog(event, sendDialog) });
        btnContact.addEventListener("click", function(event) { openDialog(event, contactDialog) });

        sendForm.addEventListener("submit", handleFormSubmit);
        sendForm.addEventListener("reset", handleFormReset);

        contactForm.addEventListener("submit", handleFormSubmit);
        contactForm.addEventListener("reset", handleFormReset);
    }


    function openDialog(event, dialog) {
        dialog.classList.remove("hidden");
        dialog.querySelector("input").focus();
        dialog.addEventListener("click", closeDialog);
    }


    function closeDialog(event) {
        var el = event.target;
        if(event.type === "click" && el.classList.contains('dialog')) {
            el.classList.add("hidden");
            el.removeEventListener("click", closeDialog);
        } else if(event.type === "submit") {
            el.parentElement.classList.add("hidden");
            el.parentElement.removeEventListener("click", closeDialog);
        }
    }


    function handleFormSubmit(event) {
        event.preventDefault();
        doFormSubmit(event.target);
        closeDialog(event);
    }


    function handleFormReset(event) {
        event.target.querySelector("textarea").innerHTML = "";
    }

    function showLoading(msg) {
        msg = msg || "Loading";

        hideLoading();
        loader = humane.loading('<i class="fa fa-spinner fa-spin"></i> ' + msg);
    }

    function hideLoading() {
        if(loader) {
            loader.remove(function() {
                loader = null;
            });
        }
    }


    function doFormSubmit(form) {
        var xhr = new XMLHttpRequest(),
            data = new FormData(),
            length = form.elements.length;

        showLoading();

        Array.prototype.forEach.call(form.elements, function(el, index) {
            if(el.tagName.toLowerCase() === "input" || el.tagName.toLowerCase() === "textarea") {
                data.append(el.name, el.value);
            }
        });

        xhr.addEventListener('load', function(event) {
            if(xhr.status === 200) {
                humane.info("Your message has been sent. Thank you for sharing.");
            } else {
                humane.error("Oops! An error occured while sending your message.");
            }
        });

        xhr.addEventListener('error', function(event) {
            humane.error("Oops! An error occured while sending your message.");
        });

        xhr.addEventListener('loadend', function(event) {
            hideLoading();
        });

        xhr.open(form.method, form.action);
        xhr.send(data);
    }


})();
